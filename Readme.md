# Flask Demo App 

## Using

Add ssh url for you server in "deploy.sh" file

Use gitlab CI/CD Pipeline for deploy you image on server

Use Branch for select Dev/Test/Prod deployment

## server configuration

You need add environment variable on you server

    PORT=8080

    DEBUG=True

### example

edit file on you server

    sudo vi /etc/profile.d/flask.sh

add variables in file /etc/profile.d/flask.sh

    export PORT=8080

    DEBUG=True
