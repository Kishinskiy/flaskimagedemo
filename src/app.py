import os

from flask import Flask, render_template

app = Flask(__name__)

PORT = int(os.getenv('PORT', 8080))
DEBUG = os.getenv('DEBUG', True)


@app.route('/')
def home():
    return render_template('base.html')



