FROM python:3.9

WORKDIR /app

COPY ./src .
COPY requirements.txt .

RUN python3 -m pip install --upgrade pip pytest && \
	python3 -m pip install -r requirements.txt

CMD gunicorn --bind 0.0.0.0:$PORT wsgi:app

